﻿using System.Web;
using System.Web.Optimization;

namespace GestionCredencialesAs400
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/materializeJS").Include(
                        "~/Content/materialize.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/dataTablesJS").Include(
                    "~/Content/DataTables/datatables.min.js"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/SiteJS").Include(
                       "~/Scripts/Site.js"));

            bundles.Add(new StyleBundle("~/bundles/materializeCSS").Include(
                      "~/Content/materialize.min.css",
                      "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/bundles/siteCSS").Include("~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/bundles/dataTablesCSS").Include(
                    "~/Content/DataTables/datatables.min.css"));
        }
    }
}
