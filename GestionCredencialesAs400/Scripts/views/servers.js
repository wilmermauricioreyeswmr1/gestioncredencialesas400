﻿var dataTable;
var serversDataTable;

var baseUrl;

$(document).ready(function () {
    MaterialInitControls();
    SetClickEvents();
    GetServers();
})

function MaterialInitControls() {

    baseUrl = root + "Servers";
    $("#ServersTable").hide();

}





function SetClickEvents() {



    $('#AddButton').on('click', function () {

        swal({
            text: 'Introduzca el nombre del Servidor',
            content: "input",
            button: {
                text: "Guardar",
                className: 'waves-effect waves-light btn green'
            },
        })
            .then(nombre => {

                Create({ "nombre": nombre });
            });

    });

    $(document).on('click', '.eliminar', function () {


        let element = $(this);
        swal({
            title: "Eliminar Servidor",
            text: "Estás seguro que deseas eliminar este registro!",
            icon: "warning",

            buttons: true,
            dangerMode: true,
        })
            .then(function (willDelete) {
                if (willDelete) {
                    Delete("Servers", "Delete", element);
                } else {
                    swal("El servidor no será eliminado");
                }
            });
    });

    $(document).on('click', '.editar', function () {


        let elementValue = serversDataTable.row($(this).parents("tr")).data();

        console.log('value', elementValue);

        swal({
            text: 'Introduzca el nombre del Servidor',
            content: {

                element: 'input',
                attributes: {
                    defaultValue: elementValue.Nombre
                }

            },

            button: {
                text: "Guardar",
                className: 'waves-effect waves-light btn green'
            },
        })
            .then(nombre => {


                Update({ nombre: nombre, id: elementValue.Id });

                //       Create({ "nombre": nombre });
            })

    });
}

function Delete(controller, action, element) {


    console.log(baseUrl + "/GetServers");

    $.ajax({
        type: "get",
        contentType: "application/json;charset=utf-8",
        url: baseUrl + "/" + action + "/" + $(element).attr("id"),

        complete: function (result) {

            console.log(result);

            if (result.status == 200) {


                serversDataTable.row($(element).parents("tr")).remove().draw();


                swal("Registro eliminado sastifactoriamente!", {
                    icon: "success",
                });



            } else {
                swal("El registro no pudo ser eliminado!", {
                    icon: "error",
                });
            }
        }
    })


}



function Update(data) {

    $.ajax({
        type: "post",
        contentType: "application/json;charset=utf-8",
        url: root + "Servers/Update",
        data: JSON.stringify(data),
        traditional: true,
        complete: function (result) {

            if (result.status == 200) {

                swal("Servidor Agregado Satisfactoriamentes!", {
                    icon: "success",
                });
                GetServers();
            }

        }
    })

}

function Create(data) {

    $.ajax({
        type: "post",
        contentType: "application/json;charset=utf-8",
        url: root + "Servers/Create",
        data: JSON.stringify(data),
        traditional: true,
        complete: function (result) {

            if (result.status == 200) {

                swal("Servidor Agregado Satisfactoriamentes!", {
                    icon: "success",
                });
                GetServers();
            }

        }
    })

}

function GetServers() {

    $.ajax({
        type: "get",
        contentType: "application/json;charset=utf-8",
        url: root + "Servers/GetServers",
        complete: function (result) {
            console.log(result);

            LoadTable(result.responseJSON)
        }
    })

}


function LoadTable(data) {


    console.log("Load table", data);
    if (serversDataTable !== undefined) {

        serversDataTable.clear();
        serversDataTable.destroy();
    }

    $("#ServersTableContainer").show();
    $("#ServersTable").show();


    serversDataTable = $("#table").DataTable({
        data: data,
        paging: false,
        searching: true,
        processing: true,
        filter: true,
        columns: [
            {
                data: 'Id',
                width: '250px', searchable: true,
            },
            {
                data: 'Nombre',
                width: '250px', searchable: true,
            }, {
                data: 'Id',
                orderable: false,
                width: '200px',
                render: function (data, type, row) {
                    return '<div class="col s12 m12 center align"><div class="col s6"><a type="button" class="editar waves-effect waves-light btn green" id="' + data + '" >Editar</a></div> <div class="col s6"><a type="button"  id="' + data + '" class="eliminar waves-effect waves-light btn red">Eliminar</a></div></div>'
                }
            }],
        initComplete: function (settings, json) {
            $("ServersTableContainer").show('slow', function () {
                $(window).resize();
            });

            SetFadingBorder('ServersTableContainer');
        }
    });
}