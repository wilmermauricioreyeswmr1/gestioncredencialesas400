﻿var dataTable;
var LibrariesDataTable;

var addLibraryModal;
var baseUrl;

$(document).ready(function () {
    MaterialInitControls();
    SetClickEvents();
    GetLibraries();
})

function MaterialInitControls() {

    baseUrl = root + "Libraries";
    $("#LibrariesTable").hide();

    $('.modal').modal();

    $('select').formSelect();

}

function SetClickEvents() {



    $("#agregar").on('click', function () {

        console.log('Librería', $(this).val())

        if ($('#formValidate').valid()) {


        }


    });


    $('#AddButton').on('click', function () {
        swal({
            text: 'Introduzca el nombre del Servidor',
            content: "input",
            button: {
                text: "Guardar",
                className: 'waves-effect waves-light btn green'
            },
        }).then(nombre => { Create({ "nombre": nombre }); });
    });

    $(document).on('click', '.eliminar', function () {

        let element = $(this);

        swal({
            title: "Eliminar Servidor",
            text: "Estás seguro que deseas eliminar este registro!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (willDelete) {
            if (willDelete) {

                Delete("Libraries", "Delete", element);



            } else { swal("El servidor no será eliminado"); }
        });
    });

    $(document).on('click', '.editar', function () {

        let elementValue = LibrariesDataTable.row($(this).parents("tr")).data();

        swal({
            text: 'Introduzca el nombre del Servidor',
            content: { element: 'input', attributes: { defaultValue: elementValue.Nombre } },
            button: { text: "Guardar", className: 'waves-effect waves-light btn green' },
        }).then(function (nombre) { Update({ nombre: nombre, id: elementValue.Id }); });
    });
}

function Delete(controller, action, element) {


    console.log(baseUrl + "/" + action + "/" + $(element).attr("id"));

    $.ajax({
        type: "get",
        contentType: "application/json;charset=utf-8",
        url: baseUrl + "/" + action + "/" + $(element).attr("id"),

        complete: function (result) {

            if (result.status == 200) {

                LibrariesDataTable.row($(element).parents("tr")).remove().draw();

                swal("Registro eliminado sastifactoriamente!", { icon: "success" });

            } else {
                swal("El registro no pudo ser eliminado!", { icon: "error" });
            }
        }
    });
}

function Update(data) {
    $.ajax({
        type: "post",
        contentType: "application/json;charset=utf-8",
        url: root + "Libraries/Update",
        data: JSON.stringify(data),
        traditional: true,
        complete: function (result) {
            if (result.status == 200) {
                swal("Servidor Agregado Satisfactoriamentes!", { icon: "success", });
                GetLibraries();
            }
        }
    });
}

function Create(data) {

    $.ajax({
        type: "post",
        contentType: "application/json;charset=utf-8",
        url: root + "Libraries/Create",
        data: JSON.stringify(data),
        traditional: true,
        complete: function (result) {

            if (result.status == 200) {

                swal("Servidor Agregado Satisfactoriamentes!", { icon: "success" });
                GetLibraries();
            }
        }
    })
}

function GetLibraries() {

    $.ajax({
        type: "get",
        contentType: "application/json;charset=utf-8",
        url: root + "Libraries/GetLibraries",
        complete: function (result) { LoadTable(result.responseJSON); }
    })
}


function LoadTable(data) {


    if (LibrariesDataTable !== undefined) {

        LibrariesDataTable.clear();
        LibrariesDataTable.destroy();
    }

    $("#LibrariesTableContainer").show();
    $("#LibrariesTable").show();


    LibrariesDataTable = $("#table").DataTable({
        data: data,
        paging: false,
        searching: true,
        processing: true,
        filter: true,
        columns: [
            {
                data: 'Id',
                width: '250px', searchable: true,
            },
            {
                data: 'Nombre',
                width: '250px', searchable: true,
            }, {
                data: 'Id',
                orderable: false,
                width: '200px',
                render: function (data, type, row) {
                    return '<div class="col s12 m12 center align"><div class="col s6"><a type="button" class="editar waves-effect waves-light btn green" id="' + data + '" >Editar</a></div> <div class="col s6"><a type="button"  id="' + data + '" class="eliminar waves-effect waves-light btn red">Eliminar</a></div></div>'
                }
            }],
        initComplete: function (settings, json) {
            $("LibrariesTableContainer").show('slow', function () {
                $(window).resize();
            });

            SetFadingBorder('LibrariesTableContainer');
        }
    });
}