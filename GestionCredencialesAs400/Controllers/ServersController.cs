﻿using GestionCredencialesAs400.Data.Repository;
using GestionCredencialesAs400.Data.Repository.Base;
using GestionCredencialesAs400.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionCredencialesAs400.Controllers
{
    public class ServersController : Controller
    {

        IRepository<Server> _repository;
        public ServersController()
        {
            _repository = new Repository<Server>();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetServers()
        {
            return Json(data: _repository.GetAll(), contentType: "application/json", behavior: JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(string nombre)
        {

            var server = new Server { Nombre = nombre };

            return Json(data: _repository.Insert(server), contentType: "application/json");

        }

        [HttpPost]
        public ActionResult Update(int id, string nombre)
        {

            var server = new Server { Id = id, Nombre = nombre };

            return Json(data: _repository.Update(server), contentType: "application/json");


        }
        public ActionResult Delete(int id)
        {
            try
            {
                Response.StatusCode = 200;

                _repository.Delete(id);

                return new JsonResult()
                {
                    Data = new
                    {
                        message = "Datos Guardados Sastifactoriamente!"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return new JsonResult()
                {
                    Data = new
                    {
                        message = "Eror al guardar los datos!"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
    }
}