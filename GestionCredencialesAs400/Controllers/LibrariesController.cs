﻿using GestionCredencialesAs400.Data.Repository;
using GestionCredencialesAs400.Data.Repository.Base;
using GestionCredencialesAs400.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionCredencialesAs400.Controllers
{
    public class LibrariesController : Controller
    {
        IRepository<Library> _libraryRepository;
        IRepository<Server> _serverRepository;
        public LibrariesController()
        {
            _libraryRepository = new Repository<Library>();
            _serverRepository = new Repository<Server>();
        }
        public ActionResult Index()
        {
            var servers = _serverRepository.GetAll();

            return View(servers);
        }

        public ActionResult GetLibraries()
        {
            return Json(data: _libraryRepository.GetAll(), contentType: "application/json", behavior: JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(string nombre)
        {
            var library = new Library { Nombre = nombre };

            return Json(data: _libraryRepository.Insert(library), contentType: "application/json");
        }

        [HttpPost]
        public ActionResult Update(int id, string nombre)
        {
            var libreria = new Library { Id = id, Nombre = nombre };

            return Json(data: _libraryRepository.Update(libreria), contentType: "application/json");
        }
        public ActionResult Delete(int id)
        {
            try
            {
                Response.StatusCode = 200;

                _libraryRepository.Delete(id);

                return new JsonResult()
                {
                    Data = new
                    {
                        message = "Datos Guardados Sastifactoriamente!"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return new JsonResult()
                {
                    Data = new
                    {
                        message = "Eror al guardar los datos!"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
    }
}
