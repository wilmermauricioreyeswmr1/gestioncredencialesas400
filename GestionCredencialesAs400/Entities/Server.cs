﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCredencialesAs400.Entities
{
    public class Server
    {
        public int Id { get; set; }
        public string Nombre {get; set; }
    }
}