﻿using GestionCredencialesAs400.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GestionCredencialesAs400.Data
{
    public class AplicationContext : DbContext
    {
        public AplicationContext() : base("GestionCredencialesAs400")
        {

        }
        DbSet<Server> servers { get; set; }
        DbSet<Library> libraries { get; set; }
    }
}