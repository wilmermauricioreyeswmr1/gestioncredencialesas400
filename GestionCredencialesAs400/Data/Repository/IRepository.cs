﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCredencialesAs400.Data.Repository
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        void Delete(int Id);
        T Insert(T item);
        T Update(T item);
        T GetById(int Id);
    }
}