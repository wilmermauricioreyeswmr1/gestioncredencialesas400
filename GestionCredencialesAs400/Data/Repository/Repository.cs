﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GestionCredencialesAs400.Data.Repository.Base
{
    public class Repository<T> : IRepository<T> where T : class
    {

        AplicationContext _context;

        DbSet<T> _table;

        public Repository()
        {
            _context = new AplicationContext();
            _table = _context.Set<T>();
        }
        public bool Delete(int Id)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<T> GetAll()
        {
            return _table.ToList();
        }

        public T GetById(int Id)
        {
            return _table.Find(Id);
        }

        public T Insert(T item)
        {
            var data = _table.Add(item);
            _context.SaveChanges();

            return data;
        }

        public T Update(T item)
        {
            var data = _table.Attach(item);
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
            return data;
            
        }

        void IRepository<T>.Delete(int Id)
        {
            var data = _table.Find(Id);
            _table.Remove(data);
            _context.SaveChanges();

        }
    }
}